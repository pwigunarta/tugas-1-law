package main

import (
	"crypto/sha1"
	"fmt"
	"os"
	"time"

	"github.com/gin-gonic/gin"
)

type Form struct {
	PackageRefs string `form:"packagerefs" binding:"required"`
}

var user = map[string][]string{
	"user1": []string{"password1", "12345", "12345"},
	"user2": []string{"password2", "12345", "12345"},
}

var userResource = map[string][]string{
	"user1": []string{"user_id1", "full_name_user1", "npm_user1"},
	"user2": []string{"user_two", "User Two", "`1909090909"},
}

var logedInUser = map[string][]string{}

func mapkey(m map[string][]string, value string) (key string, data []string, ok bool) {
	for k, v := range m {
		if v[0] == value {
			key = k
			data = v
			ok = true
			return
		}
	}
	return
}

func validateToken(t string) bool {
	timeNow := time.Now().Local().Format("2006-01-02 15:04:05")
	timeNowParse, _ := time.Parse("2006-01-02 15:04:05", timeNow)
	timeExpr, _ := time.Parse("2006-01-02 15:04:05", t)
	return timeNowParse.Before(timeExpr)
}

func main() {
	r := gin.Default()
	r.POST("/oauth/token", func(c *gin.Context) {
		username := c.PostForm("username")
		password := c.PostForm("password")
		client_id := c.PostForm("client_id")
		client_secret := c.PostForm("client_secret")

		data, ok := user[username]
		if ok && data[0] == password && client_id == data[1] && client_secret == data[2] {
			authData, ok := logedInUser[username]
			if !ok {
				t := time.Now()
				text := username + client_secret + time.Now().String()
				var sha = sha1.New()
				sha.Write([]byte(text))
				var encrypted = sha.Sum(nil)
				var encryptedString = fmt.Sprintf("%x", encrypted)

				refreshToken := text + "refresh token"
				var shaR = sha1.New()
				shaR.Write([]byte(refreshToken))
				var encryptedR = shaR.Sum(nil)
				var encryptedStringR = fmt.Sprintf("%x", encryptedR)

				logedInUser[username] = []string{encryptedString, encryptedStringR, t.Local().Add(time.Minute * 5).Format("2006-01-02 15:04:05")}

				c.JSON(200, gin.H{
					"access_token":  encryptedString,
					"expires_in":    300,
					"token_type":    "Bearer",
					"scope":         nil,
					"refresh_token": encryptedStringR,
				})
			} else {
				if !validateToken(authData[2]) {
					c.JSON(200, gin.H{
						"error":             "invalid_request",
						"Error_description": "Tolong lakukan login lagi karena token anda sudah expired",
					})
				}
			}
		} else {
			c.JSON(401, gin.H{
				"error":             "invalid_request",
				"Error_description": "ada kesalahan masbro!",
			})
		}

	})

	r.POST("/oauth/resource", func(c *gin.Context) {
		token := c.Request.Header["Authorization"][0][7:]
		userKey, loginData, ok := mapkey(logedInUser, token)
		if ok && validateToken(loginData[2]) {
			accountData := userResource[userKey]
			userData := user[userKey]
			c.JSON(200, gin.H{
				"access_token": token,
				"client_id" : userData[1],
				"user_id" : accountData[0],
				"full_name" : accountData[1],
				"npm": accountData[2],
				"expires": nil,
				"refresh_token": loginData[1],
			})
		}else{
			c.JSON(401, gin.H{
				"error":             "invalid_token",
				"Error_description": "Token Salah masbro",
			})
		}
	})

	r.POST("/oauth/refresh", func(c *gin.Context) {
		token := c.Request.Header["Authorization"][0][7:]
		refreshToken := c.PostForm("refresh_token")
		username, loginData, ok := mapkey(logedInUser, token)
		userData, _ := user[username]
		if ok && refreshToken == loginData[1] {
			t := time.Now().Local()
			text := username + userData[2] + time.Now().String()
			var sha = sha1.New()
			sha.Write([]byte(text))
			var encrypted = sha.Sum(nil)
			var encryptedString = fmt.Sprintf("%x", encrypted)
			delete(logedInUser, username)
			logedInUser[username] = []string{encryptedString, refreshToken, t.Local().Add(time.Minute * 5).Format("2006-01-02 15:04:05")}
			c.JSON(200, gin.H{
				"data": logedInUser,
			})
		}
	})

	r.POST("/oauth/logout", func(c *gin.Context) {
		token := c.Request.Header["Authorization"][0][7:]
		username, _, _ := mapkey(logedInUser, token)
		delete(logedInUser, username)
		_, ok := logedInUser[username]
		if !ok{
			c.JSON(200, gin.H{
				"status":"kamu telah logout dari sistem",
			})
		}

	})
	r.GET("/getLogedInUser", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"loged in user": logedInUser,
		})
	})
	r.Run(":" + os.Getenv("PORT"))
}
